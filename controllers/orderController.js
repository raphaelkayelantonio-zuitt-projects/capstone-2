const Order = require("../models/Order")
const bcrypt = require('bcrypt');
const auth = require('../auth');



module.exports.addOrder = async (data) => {
        if (data.isAdmin !== true) {
            let newOrder = await new Order({
                userId: data.reqBody.userId,
                productId: data.reqBody.productId,
                quantity: data.reqBody.quantity,
                amount: data.reqBody.amount
            });
            
            let total = data.reqBody.quantity * data.reqBody.amount 
            return newOrder
                .save()
                .then((order, err) => {
                    if (err) {
                        return err;
                    }else{
                        return `Product added ${order} total amount is ${total}`;
                    }
            });
        }else{
            return `Access denied`;
        }
    };


module.exports.listOrders = (userId) => {
    
        return Order
            .find({userId : userId})
            .then(result => {

                if(result.length === 0){
                    return `No existing orders`;
                
                }else{
                    return `Your current orders ${result}`;
                };
        });
    };



    module.exports.allOrders = (isAdmin) => {
        if(isAdmin === true) {
            return Order
                .find({})
                .then(result => {
                    return `Here's a list of all orders ${result}`;
                });
        }else{
            return `You must be an Admin to perform this action`;
        };
    };
