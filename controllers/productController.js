const Product = require("../models/Product");

module.exports.addProduct = (reqBody) =>{

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	})

	return newProduct.save().then((product, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}



module.exports.getAllActive = () =>{
	return Product.find({isActive:true}).then(result => result);
}


module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}

module.exports.updateProduct = (productId, reqBody) =>{
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.archiveProduct = (productId) =>{
	let updateActiveField = {
		isActive : false
	}

	return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}