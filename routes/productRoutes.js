const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

router.post("/", auth.verify, (req,res) =>{

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permission on this page!");
	}
	
})


router.get("/", (req, res) =>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

router.get("/:productId", (req,res)=>{
	console.log(req.params.productId);

	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId", auth.verify, (req, res)=>{
	productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})

router.patch("/:productId/archive", auth.verify, (req, res) =>{
	productController.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})
module.exports = router;