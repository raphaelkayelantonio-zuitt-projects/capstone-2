const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth')


router.post("/checkout", auth.verify, (req, res) => {
        const data = {
            isAdmin: auth.decode(req.headers.authorization).isAdmin,
            reqBody: req.body,
        };
        orderController
            .addOrder(data)
            .then(resultFromController => 
                res.send(resultFromController));
    });


    router.get('/userorder', auth.verify, (req, res) => {
        
        const userId = auth.decode(req.headers.authorization).id;
            
        orderController
            .listOrders(userId)
            .then(resultFromController => 
                res.send(resultFromController))
    });



    router.get('/all', auth.verify, (req, res) => {
        const isAdmin = auth.decode(req.headers.authorization).isAdmin;
        orderController.allOrders(isAdmin).then(resultFromController =>
                res.send(resultFromController))
    });

module.exports = router;